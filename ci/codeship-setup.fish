#!/usr/bin/env fish
read --prompt "echo 'Repo URL (including credentials): '" -l repo
git clone $repo r
cd r

echo Number of builds:
read -l num

git config --global user.email (random)@example.com
git config --global user.name (random)

for i in (seq $num)
  date > date.txt
  random > rand.txt
  git add .
  git commit -m (random)
  git push
end
